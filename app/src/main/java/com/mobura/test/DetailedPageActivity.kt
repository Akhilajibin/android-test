package com.mobura.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detailed_page.*
import kotlinx.coroutines.Job

class DetailedPageActivity : AppCompatActivity() {
    private lateinit var item: ApodList
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_page)
        if (intent.hasExtra("adop")) {
            item = intent.getParcelableExtra("adop") as ApodList
            Log.d("tit",item.title)
        }
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
       toolbar.setNavigationIcon(R.drawable.ic_arrows)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        // Display application icon in the toolbar
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        Glide.with(applicationContext).load(item.hdurl).centerCrop().placeholder(R.drawable.ic_nasa_logos)
            .error(R.drawable.ic_nasa_logos)
            .into(img)
        heading.text=item.title
        date.text=item.date
        details.text=item.explanation
    }
}