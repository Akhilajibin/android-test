package com.mobura.test

enum class Status() {
    SUCCESS,
    ERROR,
    LOADING
}