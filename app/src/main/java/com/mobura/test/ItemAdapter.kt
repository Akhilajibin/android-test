package com.mobura.test

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.apoditem.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class ItemAdapter(val context: Context, var jobs: List<ApodList>, var itemClicked: itemClick) :

    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    var pos:Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = ItemViewHolder(

        LayoutInflater.from(parent.context).inflate(R.layout.apoditem, parent, false)
    )

    override fun getItemCount() = jobs.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        pos=position
        holder.setIsRecyclable(false)

        holder.bind(jobs[position])

    }

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val root = view
        private  val title=view.title
        private  val img=view.userimg
        private  val date=view.date


        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(item: ApodList) {
            title.text=item.title


            var dates = item.date
            var spf = SimpleDateFormat("yyyy-MM-dd")
            val newDate: Date = spf.parse(dates)
            spf = SimpleDateFormat("MMM dd, yyyy")
            dates = spf.format(newDate)
            date.text=dates.toString()
            if(item.media_type=="image"){

                    Glide.with(context).load(item.url)
                        .circleCrop()
                        .error(R.drawable.ic_nasa_logos)
                        .into(img)
            }
            else{
                Glide.with(context).load(R.drawable.ic_nasa_logos).centerCrop()
                    .error(R.drawable.ic_nasa_logos)
                    .into(img)
            }

            root.setOnClickListener {
                itemClicked.selectedItem(item)
//
            }

        }
    }


}