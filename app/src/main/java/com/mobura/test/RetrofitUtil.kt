package com.mobura.test

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

object RetrofitUtil {

    val API_BASE_URL = "https://api.nasa.gov/planetary/"


    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())
            .build()
    }

    fun getRetrofitService(): RestApiService {
        return getRetrofit().create(RestApiService::class.java)
    }

    private fun getOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(1000, TimeUnit.SECONDS)
        httpClient.readTimeout(5, TimeUnit.MINUTES)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder().method(original.method, original.body)
            //TODO builder.header("token", "Token You want to send")
            val request = builder.build()
            chain.proceed(request)
        }
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        return httpClient.build()
    }

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Resource<T> {
        return try {
            Resource.loading<T>()
            val response = call.invoke()
            if (response.isSuccessful) {
                Resource.success(response.body()!!)
            } else {
                Resource.error("Oops something went wrong, Please try again")
            }
        } catch (exception: Exception) {
            when (exception) {
                is IOException -> Resource.error("Please check internet connectivity")
                else -> Resource.error("Something went wrong, Please try again")
            }
        }
    }

}