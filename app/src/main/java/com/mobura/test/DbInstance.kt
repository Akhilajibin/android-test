package com.mobura.test

import android.content.Context

object DbInstance {
    fun provideItemDataSource(context: Context): NasaDao {
        val database = NasaDatabase.getInstance(context)
        return database.nasaDao()
    }
}