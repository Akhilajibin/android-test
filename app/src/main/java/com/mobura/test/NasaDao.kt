package com.mobura.test

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface NasaDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
     fun insertItems(jobs: List<ApodList>)

    @Query("DELETE FROM Apod")
     fun deleteAllItems(): Int


    @Query("SELECT * FROM Apod")
     fun getAllItems(): List<ApodList>
}
