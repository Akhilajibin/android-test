package com.mobura.test

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ListingPage : AppCompatActivity(),itemClick {
    var coroutineJob = kotlinx.coroutines.Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + coroutineJob)

    private var items: MutableList<ApodList> = mutableListOf()
    private lateinit var viewModel: ItemViewModel
    private var itemAdapter: ItemAdapter? = null
    private var itemDao: NasaDao? = null
    private lateinit var listItem: List<ApodList>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        itemDao = DbInstance.provideItemDataSource(applicationContext)

        viewModel = ViewModelProvider(this).get(ItemViewModel::class.java)


        // using toolbar as ActionBar
        setSupportActionBar(toolbar)


        // Display application icon in the toolbar
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        //  supportActionBar!!.setLogo(R.drawable.app_icon)
        supportActionBar!!.setDisplayUseLogoEnabled(true)
        if (checkConnectivity(applicationContext)) {
            getApodList()
        } else {
            viewModel.getAllOfflinJob()
            viewModel.offline.observe(this, androidx.lifecycle.Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        items.clear()
                      //  viewModel.deleteItems()
                        progressbar.visibility=View.GONE
                        it.data?.let { jobs -> this.items.addAll(jobs) }
                        itemAdapter?.notifyDataSetChanged()
                        intializeAdapter()
                    }
                    Status.ERROR->
                    {
                        progressbar.visibility=View.GONE
                        Toast.makeText(applicationContext,"Something went wrong",Toast.LENGTH_SHORT).show()
                    }

                }
            })


        }
    }
    fun getApodList(){
       viewModel.topics.observe(this,androidx.lifecycle.Observer {
           when (it.status) {
               Status.SUCCESS -> {


                   items.clear()
                   viewModel.deleteItems()
                   progressbar.visibility=View.GONE
                   it.data?.let { jobs -> this.items.addAll(jobs) }
                   itemAdapter?.notifyDataSetChanged()
                   intializeAdapter()

               }
               Status.LOADING->
                       {
                          progressbar.visibility=View.VISIBLE
                       }
               Status.ERROR->
               {
                   progressbar.visibility=View.GONE
                   Toast.makeText(applicationContext,"Something went wrong",Toast.LENGTH_SHORT).show()
               }
           }
       })
    }
    fun intializeAdapter(){

            Log.d("jobsize inside adapter",  items.size.toString())
            itemAdapter = ItemAdapter(applicationContext, items,this)
        listitem.apply {
                layoutManager = LinearLayoutManager(applicationContext)
                adapter = itemAdapter
            }


    }

    override fun selectedItem(item: ApodList) {
        val intent = Intent(applicationContext, DetailedPageActivity::class.java)

        intent.putExtra("adop", item as Parcelable)
        //context.startActivityForResult(intent,121)
        startActivity(intent)
    }
    fun checkConnectivity(context: Context): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo

        if(activeNetwork?.isConnected!=null){
            return activeNetwork.isConnected
        }
        else{
            return false
        }
    }
}

