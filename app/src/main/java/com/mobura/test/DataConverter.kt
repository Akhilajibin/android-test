package com.mobura.test

import androidx.room.TypeConverter
import com.google.gson.Gson

class DataConverter {
    @TypeConverter
    fun fromJobProcess(jobProcess: ApodList): String{
        return Gson().toJson(jobProcess)
    }
}