package com.mobura.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
//
        Glide.with(applicationContext)
            .load(R.drawable.ic_nasa_logos)
            .apply( RequestOptions().override(400, 400))
            .into(nasalogo)
        Handler().postDelayed({
            val intent = Intent(this, ListingPage::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}