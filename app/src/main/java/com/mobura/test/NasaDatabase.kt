package com.mobura.test


import android.content.Context
import androidx.room.*


@Database(entities = [ApodList::class], version = 9)
@TypeConverters(DataConverter::class)
abstract class NasaDatabase : RoomDatabase() {
    abstract fun nasaDao():NasaDao
    companion object {
        @Volatile
        private var INSTANCE: NasaDatabase? = null
        fun getInstance(context: Context): NasaDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(
                        context
                    ).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, NasaDatabase::class.java, "Nasa.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}