package com.mobura.test

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ItemRepositary(val application: Application) {
    private var itemsMutableLiveData = MutableLiveData<Resource<List<ApodList>>>()
    var coroutineJob = kotlinx.coroutines.Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + coroutineJob)
    private var itemDao: NasaDao = DbInstance.provideItemDataSource(application.applicationContext)
    fun insertItems(jobs: List<ApodList>) {
        coroutineScope.launch {
            itemDao.insertItems(jobs)
        }
    }
    fun deleteAllItems() {
        coroutineScope.launch {
            itemDao.deleteAllItems()
        }
    }
    fun getAllJobs(): LiveData<Resource<List<ApodList>>> {
        coroutineScope.launch {
            val items = itemDao.getAllItems()
            if (items.isNotEmpty()) {
                itemsMutableLiveData.value = Resource.success(items)
            } else {
                itemsMutableLiveData.value = Resource.error("No Item Found")
            }
        }
        return itemsMutableLiveData
    }
    fun getJobs(): MutableLiveData<Resource<List<ApodList>>> {
        coroutineScope.launch {
            try {
                itemsMutableLiveData.value = Resource.loading()
                val response = RetrofitUtil.getRetrofitService()
                    .getItems("3fffTdegzafPItxihdbL3ezC06rdTBMlL5iBhygs", "10")
                if(response.isSuccessful){

                    itemsMutableLiveData.value= Resource.success(response.body()!!)
                    insertItems(response.body()!!)
                }
                else{
                  //  Toast.makeText(applicationContext,"Error", Toast.LENGTH_SHORT ).show()
                    itemsMutableLiveData.value = Resource.error("No Item Found")
                }


            } catch (exception: Exception) {

            }
        }
        return itemsMutableLiveData
    }
}

