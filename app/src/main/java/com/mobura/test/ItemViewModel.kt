package com.mobura.test

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ItemViewModel(application: Application) : AndroidViewModel(application) {

    private val itemRepository = ItemRepositary(application)
    val topics: LiveData<Resource<List<ApodList>>> get() = itemRepository.getJobs()
    fun deleteItems()=itemRepository.deleteAllItems()
    fun getAllOfflinJob()=itemRepository.getAllJobs()
    val offline: LiveData<Resource<List<ApodList>>> get() =itemRepository.getAllJobs()
}