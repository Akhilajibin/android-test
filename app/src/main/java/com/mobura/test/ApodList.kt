package com.mobura.test
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Apod")
data class ApodList(
    val copyright: String?,
    val date: String,
    val explanation: String,
    val hdurl: String?,
    val media_type: String,
    val service_version: String,
    val title: String,
    val url: String?
): Parcelable{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
