package com.mobura.test

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RestApiService {
    @GET("apod")
    suspend fun getItems(@Query("api_key") apikey: String, @Query("count") count: String ):Response<List<ApodList>>
//    suspend fun getItems(@Url url: String): Response<List<Job>>
}